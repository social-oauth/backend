var express = require('express');
var router = express.Router();

module.exports = function(app) {
  /* GET home page. */
  router.get('/', function(req, res, next) {
    res.render('index', {user: req.session.passport});
  });


  app.use('/auth', require('./auth'));
  app.use('/users', require('./users'));
}
