var express = require('express');
var router = express.Router();
var FacebookService = require('../services/facebookService');

router.use((req,res,next) => {
  if(!req.session.passport) {
    return res.json({error: 'You have to login to acces this page!'});
  }
  next();
})

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send(req.session.passport);
});

router.get('/picture', function(req, res, next) {
  facebookService = new FacebookService();
  if(!req.session.passport.user.social.facebook) {
    return res.json({error: 'You have to add your facebook account to see the picture'});
  }
  facebookService.getUserImage('large', req.session.passport.user.social.facebook.token, (err, data, response) => {
    if(err) return res.json(err);
    res.json(data);
  })
});

router.get('/posts', function(req, res, next) {
  facebookService = new FacebookService();
  if(!req.session.passport.user.social.facebook) {
    return res.json({error: 'You have to add your facebook account to see the posts'});
  }
  facebookService.getUserPosts(req.session.passport.user.social.facebook.token, (err, data, response) => {
    if(err) return res.json(err);
    res.json(data);
  })
});

module.exports = router;

