var express = require('express');
var router = express.Router();
var passport = require('passport');

router.get('/google', passport.authenticate('google', {scope: ['profile', 'email']}));
router.get('/google/callback', passport.authenticate('google', {
    successRedirect: '/users',
    failureRedirect: '/'
}));

router.get('/facebook', passport.authenticate('facebook', {scope: ['email', 'user_photos', 'user_posts']}));
router.get('/facebook/callback', passport.authenticate('facebook', {
    successRedirect: '/users',
    failureRedirect: '/'
}));

router.get('/github', passport.authenticate('github'));
router.get('/github/callback', passport.authenticate('github', {
    successRedirect: '/users',
    failureRedirect: '/'
}));

module.exports = router;
