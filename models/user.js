var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    displayName: String,
    social: {
        facebook: Object,
        google: Object,
        github: Object
    }
})

module.exports = mongoose.model('User', userSchema);