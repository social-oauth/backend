Simple Social Login
===

Social Login page using Passport.js and Google, Facebook, Github strategies for Node.js

After login via Facebook you can get JSON with your profile image or posts data.

The demo available here: https://oauth-passport-backend.herokuapp.com/