var passport = require('passport');
var GithubStrategy = require('passport-github').Strategy;
var config = require('config-yml');
var User = require('../../models/user');
var userService = require('../../services/userService');

module.exports = function() {
    passport.use(new GithubStrategy({
        clientID: config.social.github.clientID,
        clientSecret: config.social.github.clientSecret,
        callbackURL: `${config.general.root_url}/auth/github/callback`,
        passReqToCallback: true
      },
      function(req, accessToken, refreshToken, profile, done) {
        let query = { 'social.github.id': profile.id }
        if(req.session.passport) {
          query = {};
          let socialProvider = Object.keys(req.session.passport.user.social)[0];
          query[`social.${socialProvider}.id`] = req.session.passport.user.social[socialProvider].id;
        }
        
        User.findOne(query).then(
          (user) => {
            let userTemp = userService.createUserFrom('github', {user: profile, token: accessToken});
            if(user) {
              user.social.github = userTemp.social.github;
            }else {
              user = userTemp;
            }
            user.save();
            done(null, user)
          },
          (error) => {
            done(error, null);
          }
        )
      }
    ));
}