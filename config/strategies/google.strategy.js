var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var config = require('config-yml');
var User = require('../../models/user');
var userService = require('../../services/userService');

module.exports = function() {
    passport.use(new GoogleStrategy({
        clientID: config.social.google.clientID,
        clientSecret: config.social.google.clientSecret,
        callbackURL: `${config.general.root_url}/auth/google/callback`,
        passReqToCallback: true
    },
    function(req, accessToken, refreshToken, profile, done) {
        let query = { 'social.google.id': profile.id }
        if(req.session.passport) {
            query = {};
            let socialProvider = Object.keys(req.session.passport.user.social)[0];
            query[`social.${socialProvider}.id`] = req.session.passport.user.social[socialProvider].id;
        }
        
        User.findOne(query).then(
            (user) => {
                let userTemp = userService.createUserFrom('google', {user: profile, token: accessToken});
                if(user) {
                    user.social.google = userTemp.social.google;
                }else {
                    user = userTemp;
                }
                user.save();
                done(null, user)
            },
            (error) => {
                done(error, null);
            }
        )
        }
    ))
}