var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var userService = require('../../services/userService');
var User = require('../../models/user');
var config = require('config-yml');

module.exports = function() {
    passport.use(new FacebookStrategy({
        clientID: config.social.facebook.clientID,
        clientSecret: config.social.facebook.clientSecret,
        callbackURL: `${config.general.root_url}/auth/facebook/callback`,
        passReqToCallback: true
      },
      function(req, accessToken, refreshToken, profile, done) {
        let query = { 'social.facebook.id': profile.id }
        if(req.session.passport) {
          query = {};
          let socialProvider = Object.keys(req.session.passport.user.social)[0];
          query[`social.${socialProvider}.id`] = req.session.passport.user.social[socialProvider].id;
        }

        User.findOne(query).then(
          (user) => {
            let userTemp = userService.createUserFrom('facebook', {user: profile, token: accessToken});
            if(user) {
              user.social.facebook = userTemp.social.facebook;
            }else {
              user = userTemp;
            }
            user.save();
            done(null, user)
          },
          (error) => {
            done(error, null);
          }
        )
      }
    ));
}