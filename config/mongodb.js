var mongoose = require('mongoose');

module.exports = function(app) {
    mongoose.connect('mongodb://test:test123@ds139883.mlab.com:39883/oauth-social', {useNewUrlParser: true});
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'Database connection error:'));
    db.once('open', function() {
        console.log('Connected to database successfully!')
    });
}

