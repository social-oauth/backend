module.exports = function(app) {
    require('./session')(app);
    require('./passport')(app);
    require('./mongodb')(app);
}