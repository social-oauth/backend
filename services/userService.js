var User = require('../models/user');

class UserService {
    
    createUserFrom(sourceType, data) {
        switch(sourceType) {
            case 'facebook': return this.createUserFromFacebook(data);
            case 'google': return this.createUserFromGoogle(data);
            case 'github': return this.createUserFromGithub(data);
            default: throw 'Unknown sourceType provided. User has not been created.'
        }
    }

    createUserFromFacebook(data) {
        return new User({
            displayName: data.user.displayName,
            social: {
                facebook: {
                    token: data.token,
                    id: data.user.id
                }
            }
        })
    }

    createUserFromGoogle(data) {
        return new User({
            displayName: data.user.displayName,
            social: {
                google: {
                    token: data.token,
                    id: data.user.id
                }
            }
        })
    }

    createUserFromGithub(data) {
        return new User({
            displayName: data.user.displayName,
            social: {
                github: {
                    token: data.token,
                    id: data.user.id
                }
            }
        })
    }
}
module.exports = new UserService();