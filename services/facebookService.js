var OAuth = require('oauth').OAuth2;
var config = require('config-yml');

class FacebookService
{
    constructor() {
        this.oauth = new OAuth(
            config.social.facebook.clientID,
            config.social.facebook.clientSecret,
            "https://graph.facebook.com", null, "oauth2/token", null)
    }
    getUserPosts(token, callback) {
        this.oauth.get('https://graph.facebook.com/v3.2/me/posts', token, callback);
    }
    getUserImage(type, token, callback) {
        this.oauth.get(`https://graph.facebook.com/v3.2/me/picture?type=${type}&redirect=false`, token, callback);
    }
}

module.exports = FacebookService;